from emphysema import db
from emphysema.patients.model import Patient
from emphysema.results.model import ResultOfTest


def get_result_for_patient(petient_id: int, page: int, per_page: int):
    return db.session.query(ResultOfTest, Patient). \
        select_from(ResultOfTest).join(Patient). \
        filter_by(id=petient_id). \
        paginate(page=page, per_page=per_page)


def get_results(page: int, per_page: int):
    return db.session.query(ResultOfTest, Patient). \
        select_from(ResultOfTest).join(Patient). \
        paginate(page=page, per_page=per_page)

def get_x_ray_to_result(id: int):
    return ResultOfTest.query\
        .get(id)


def save(result):
    db.session.add(result)
    db.session.flush()
    result_id = result.id
    db.session.commit()
    return result_id
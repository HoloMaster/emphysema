import os
import secrets
from flask import current_app


def save_x_ray(x_ray):
    randomized_name = secrets.token_hex(8)
    _, ext = os.path.splitext(x_ray.filename)
    x_ray_to_be_saved = randomized_name + ext
    path = os.path.join(current_app.root_path, 'static/x_rays', x_ray_to_be_saved)
    x_ray.save(path)
    randomized_name = randomized_name + ext
    return path, x_ray.filename, randomized_name

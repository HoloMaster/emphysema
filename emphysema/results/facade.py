import logging
import os
from datetime import datetime

import requests

from emphysema.diseases.manager import DiseaseService
from emphysema.diseases.model import Disease
from emphysema.expertPredictor.expertClass import Symptoms, SymptomsToDisease, Diseases
from emphysema.expertPredictor.handler import Handler
from emphysema.patients.manager import PatientService
from emphysema.patients.utils import compute_bmi, compute_fe1vc
from emphysema.results.model import ResultOfTest
from emphysema.results.service import save
from emphysema.results.utils import save_x_ray
from emphysema.services.crudService import BaseService
from emphysema.symptoms.manager import SymptomService
from emphysema.symptoms.model import Symptom

baseService = BaseService()
patientsService = PatientService()
diseaseService = DiseaseService()
symptomService = SymptomService()


class ResultFacade:

    @staticmethod
    def send_to_prediction(form, id):
        path, name, generated_name = None, None, None
        if form.rtg_scan:
            path, name, generated_name = save_x_ray(form.rtg_scan.data)
        result = ResultOfTest(patient_id=id, pathToXray=path, originalName=name, generatedName=generated_name)
        returned_id = save(result)
        try:
            response = requests.post(os.environ.get('URL_TO_NN'), data=form.rtg_scan.data)
            for r in response:
                if r.get("emphysema"):
                    result = baseService.get_by_id(ResultOfTest, id=returned_id)
                    result.emphysemaNN = r.get("emphysema")
                    result.dateOfEnd = datetime.utcnow()
                    baseService.update()
            return True
        except Exception as e:
            logging.exception("During send image to NN occured exception", e)
            return False

    @staticmethod
    def send_to_prediction_expert_system(patient, id, form):
        path, name, generated_name = None, None, None
        if form.rtg_scan.data:
            path, name, generated_name = save_x_ray(form.rtg_scan.data)
        result = ResultOfTest(patient_id=id, pathToXray=path, originalName=name, generatedName=generated_name)
        returned_id = save(result)
        handler = Handler(patient=patient, results=result)
        symptoms_for_emphysema = symptomService.get_symptoms_for_disease_patient(patient_id=patient.id,
                                                                                 diseases=diseaseService.get_by_patient(
                                                                                     name=patient.id))
        diseases = diseaseService.get_all(Disease)
        try:
            response = requests.post(os.environ.get('URL_TO_NN'), data=form.rtg_scan.data)
            for r in response:
                if r.get("emphysema"):
                    result = baseService.get_by_id(ResultOfTest, id=returned_id)
                    result.emphysemaNN = r.get("emphysema")
        except Exception as e:
            logging.exception("During send image to NN occured exception", e)

        symptoms_for_emphysema_fact = Symptoms(val=symptoms_for_emphysema)
        diseases_fact = [Diseases(disease=disease.name_of_disease) for disease in diseases]
        num_and_disease_fact = [SymptomsToDisease(num=symptomService.get_symptoms_for_disease(disease.name_of_disease),
                                                  disease=disease.name_of_disease) for disease in diseases]
        ptf_fev1_from_patient = compute_fe1vc(fev1=patient.ptf_fev1,fcv=patient.ptf_fcv)
        ptf_dlco_from_patient = patient.ptf_dlco
        pulse_oxymetry_from_patient = patient.pulse_oxymetry
        patient_bmi = compute_bmi(weight=patient.weight, height=patient.height)

        result_of_testing = handler.notify(symptomsForEmphysemaFact=symptoms_for_emphysema_fact,
                                           diseasesFact=diseases_fact, numAndDiseaseFact=num_and_disease_fact,
                                           ptfFev1FromPatient=ptf_fev1_from_patient,
                                           ptfDlcoFromPatient=ptf_dlco_from_patient,
                                           pulseOxymetryFromPatient=pulse_oxymetry_from_patient, patientBMI=patient_bmi,
                                           smoker=patient.smoker,
                                           result=result.emphysemaNN)


        comment = result_of_testing
        result.emphysemaES = comment
        result.dateOfEnd = datetime.utcnow()
        baseService.update()

    @staticmethod
    def prepare_data_for_form(form, id, patient):
        patient_with_diseases = patientsService.get_all_for_user(id)
        if patient_with_diseases:
            diseases = diseaseService.get_all(class_=Disease)
            symptoms = symptomService.get_all(class_=Symptom)
            form.set_patient_for_update(form=form, patient=patient_with_diseases, diseases=diseases, symptoms=symptoms)
        else:
            form.set_patient_for_update_without_disease(form=form, patient=patient)
            form.set_diseases_in_creation_form(form, diseases=diseaseService.get_all(class_=Disease))

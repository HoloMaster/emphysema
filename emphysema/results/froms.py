from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileAllowed
from wtforms import StringField, DecimalField, BooleanField
from wtforms.fields import SubmitField, IntegerField
from wtforms.validators import Length, DataRequired, Optional
from wtforms.widgets import html5 as widgetshtml5

from emphysema.forms.customForms.selectMultipleFiledCustom import SelectMultipleFieldCustom
from emphysema.patients.model import Patient


class PredictionForm(FlaskForm):
    nameOfPatient = StringField("Jméno pacienta", validators=[DataRequired(), Length(min=3, max=20)])

    height = DecimalField("Výška", widget=widgetshtml5.NumberInput(1, 0, 250), validators=[DataRequired()])

    weight = DecimalField("Váha", widget=widgetshtml5.NumberInput(1, 0, 250), validators=[DataRequired()])

    age = DecimalField("Věk", widget=widgetshtml5.NumberInput(1, 0, 150), validators=[DataRequired()])

    cigarets_per_year = DecimalField(
        "Krabičkoroky (Krabičky za den * roky jak dlouho kouří) / prázné pokud je pacient nekuřák",
        widget=widgetshtml5.NumberInput(1, 0, 150), validators=[DataRequired()])

    deficit_alfa_1 = BooleanField("Deficit alfa-1-antitrypsináza",validators=[Optional()])

    breath_frequency = DecimalField("Dechová frekvence (počet dechů za minutu)",
                                    widget=widgetshtml5.NumberInput(1, 0, 100), validators=[Optional()])

    pulse_oxymetry = DecimalField("SpO2", widget=widgetshtml5.NumberInput(1, 0, 100), validators=[Optional()])

    ptf_fev1 = DecimalField("FEV1 (v litrech)", widget=widgetshtml5.NumberInput(1, 0, 100), validators=[Optional()])

    ptf_fcv = DecimalField("FCV  (v litrech)", widget=widgetshtml5.NumberInput(1, 0, 20), validators=[Optional()])

    ptf_dlco = DecimalField("DLCO", widget=widgetshtml5.NumberInput(1, 0, 100), validators=[Optional()])

    smoker = BooleanField("Kuřák", validators=[Optional()])

    otherDiseases = StringField("Nemoci k predikci")

    submit = SubmitField("Predikovat")


class SendToPredictionExpertSystem(FlaskForm):
    nameOfPatient = StringField("Jméno pacienta", validators=[DataRequired(), Length(min=3, max=20)])

    height = DecimalField("Výška", widget=widgetshtml5.NumberInput(1, 0, 250), validators=[DataRequired()])

    weight = DecimalField("Váha", widget=widgetshtml5.NumberInput(1, 0, 250), validators=[DataRequired()])

    age = DecimalField("Věk", widget=widgetshtml5.NumberInput(1, 0, 150), validators=[DataRequired()])

    cigarets_per_year = DecimalField(
        "Krabičkoroky (Krabičky za den * roky jak dlouho kouří) / prázné pokud je pacient nekuřák",
        widget=widgetshtml5.NumberInput(1, 0, 150), validators=[Optional()])

    deficit_alfa_1 = BooleanField("Deficit alfa-1-antitrypsináza",validators=[Optional()])

    breath_frequency = DecimalField("Dechová frekvence (počet dechů za minutu)",
                                    widget=widgetshtml5.NumberInput(1, 0, 100), validators=[Optional()])

    pulse_oxymetry = DecimalField("SpO2", widget=widgetshtml5.NumberInput(1, 0, 100), validators=[Optional()])

    ptf_fev1 = DecimalField("FEV1 (v litrech)", widget=widgetshtml5.NumberInput(1, 0, 20), validators=[Optional()])

    ptf_fcv = DecimalField("FCV  (v litrech)", widget=widgetshtml5.NumberInput(1, 0, 20), validators=[Optional()])

    ptf_dlco = DecimalField("DLCO", widget=widgetshtml5.NumberInput(1, 0, 100), validators=[Optional()])

    smoker = BooleanField("Kuřák", validators=[Optional()])

    otherDiseases = SelectMultipleFieldCustom("Nemoci k predikci", validators=[])

    symptoms = SelectMultipleFieldCustom("Symptomy kterými pacient trpí", validators=[])

    rtg_scan = FileField('Nahrat rtg snimek', validators=[FileAllowed(['jpg', 'png'])])

    submit = SubmitField('Odeslat k predikci')

    def set_this_user(self, patient, form):
        form.nameOfPatient.data = patient.nameOfPatient
        form.height.data = patient.height
        form.weight.data = patient.weight
        form.pulse_oxymetry.data = patient.pulse_oxymetry
        form.ptf_fev1.data = patient.ptf_fev1
        form.ptf_dlco.data = patient.ptf_dlco
        form.smoker.data = patient.smoker
        form.age.data = patient.age
        form.ptf_fcv.data = patient.ptf_fcv
        form.deficit_alfa_1.data = patient.deficit_alfa_1
        form.cigarets_per_year.data = patient.cigarets_per_year
        form.breath_frequency.data = patient.breath_frequency
        # form.otherDiseases.data = diseases

    def set_patient_for_update(self, form, patient: Patient, diseases, symptoms):
        form.nameOfPatient.data = patient[0].nameOfPatient
        form.height.data = patient[0].height
        form.weight.data = patient[0].weight
        form.pulse_oxymetry.data = patient[0].pulse_oxymetry
        form.ptf_fev1.data = patient[0].ptf_fev1
        form.ptf_dlco.data = patient[0].ptf_dlco
        form.smoker.data = patient[0].smoker
        form.age.data = patient[0].age
        form.ptf_fcv.data = patient[0].ptf_fcv
        form.deficit_alfa_1.data = patient[0].deficit_alfa_1
        form.cigarets_per_year.data = patient[0].cigarets_per_year
        form.breath_frequency.data = patient[0].breath_frequency
        form.otherDiseases.choices = [(d.name_of_disease, d.name_of_disease) for d in diseases]
        # needed to prepopulate with selected
        form.otherDiseases.process_data(d.disease_id for d in patient[0].otherDiseases)
        form.symptoms.choices = [(s.name_of_symptom, s.name_of_symptom) for s in symptoms]
        # needed to prepopulate with selected
        form.symptoms.process_data(s.symptom_id for s in patient[0].symptoms)

    def set_patient_for_update_without_disease(self, form, patient):
        form.nameOfPatient.data = patient.nameOfPatient
        form.height.data = patient.height
        form.weight.data = patient.weight
        form.smoker.data = patient.smoker
        form.age.data = patient.age
        form.ptf_fcv.data = patient.ptf_fcv
        form.deficit_alfa_1.data = patient.deficit_alfa_1
        form.cigarets_per_year.data = patient.cigarets_per_year
        form.breath_frequency.data = patient.breath_frequency

    def set_diseases_in_creation_form(self, form, diseases):
        form.otherDiseases.choices = [(d.id, d.name_of_disease) for d in diseases]


class SendToPredcitionForm(FlaskForm):
    patient_id = IntegerField()

    rtg_scan = FileField('Nahrat rtg snimek', validators=[DataRequired(), FileAllowed(['jpg', 'png'])])

    submit = SubmitField('Odeslat k predikci')

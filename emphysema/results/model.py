from datetime import datetime
from emphysema import db


class ResultOfTest(db.Model):
    __tablename__ = 'tbl_resultOfTest'

    id = db.Column(db.Integer, primary_key=True)

    dateOfCreation = db.Column(db.DateTime, nullable=False, default=datetime.utcnow())

    dateOfEnd = db.Column(db.DateTime)

    emphysemaNN = db.Column(db.Numeric(precision=10,scale=2,decimal_return_scale=2))

    emphysemaES = db.Column(db.String(255), nullable=True)

    pathToXray = db.Column(db.String(255), nullable=False)

    originalName = db.Column(db.String(255), nullable=False)

    generatedName = db.Column(db.String(255), nullable=False)

    patient_id = db.Column(db.Integer, db.ForeignKey('tbl_patient.id'), nullable=True)

    def __init__(self, patient_id, pathToXray, originalName, generatedName):
        self.patient_id = patient_id
        self.pathToXray = pathToXray
        self.originalName = originalName
        self.generatedName = generatedName

    def __repr__(self):
        return f"Results('{self.dateOfCreation}', '{self.dateOfEnd}', '{self.emphysemaNN}', '{self.emphysemaES}', " \
               f"'{self.patient_id}')"

from flask import Blueprint, render_template, request, redirect, url_for, flash
from flask_login import login_required

from emphysema.diseases.manager import DiseaseService
from emphysema.patients.facade import PatientFacade
from emphysema.patients.manager import PatientService
from emphysema.patients.model import Patient
from emphysema.results.facade import ResultFacade
from emphysema.results.froms import SendToPredictionExpertSystem, SendToPredcitionForm
from emphysema.results.service import get_results, get_x_ray_to_result
from emphysema.services.crudService import BaseService
from emphysema.services.utils import getIdFromUrl, getNameFromUrl
from emphysema.symptoms.manager import SymptomService

resultsBp = Blueprint('resultsBp', __name__)

baseService = BaseService()
patientsService = PatientService()
diseaseService = DiseaseService()
symptomService = SymptomService()
resultFacade = ResultFacade()
patientFacade = PatientFacade()


@resultsBp.route('/results')
@login_required
def results():
    page = request.args.get('page', 1, type=int)
    per_page = request.args.get('per_page', 20, type=int)
    results_paged = get_results(page, per_page)
    return render_template("result/results.html", title="Výsledky", results=results_paged)


@resultsBp.route('/result_x_ray/<id>')
@login_required
def result_x_ray(id):
    result = get_x_ray_to_result(id=id)
    if result.generatedName:
        path_to_xray = '/static/x_rays/' + result.generatedName
        return render_template("result/x_ray.html", image=path_to_xray, old_image_name=result.originalName)
    else:
        return render_template("result/no_x_ray_image.html")


@resultsBp.route('/sendToPrediction/<id>', methods=['GET', 'POST'])
@login_required
def send_to_prediction(id):
    patient_id = getIdFromUrl(id)
    form = SendToPredcitionForm(patient_id=patient_id)
    patient = baseService.get_by_id(Patient, patient_id)
    if form.validate_on_submit():
        result = resultFacade.send_to_prediction(form=form, id=patient_id)
        if result:
            return redirect(url_for('patientsBp.patient', id=patient_id))
        else:
            flash(f'Pacient {patient.nameOfPatient} nebyl úspěšně odeslán k predikci', 'danger')
            return redirect(url_for('patientsBp.patient', id=patient_id))
    return render_template("prediction/sendToPrediction.html", title="Predikce", form=form,
                           patient_id=getNameFromUrl(id)[1], nameOfPatient=patient.nameOfPatient)


@resultsBp.route('/sendToPredictionExpertSystem/<id>', methods=['GET', 'POST'])
@login_required
def send_to_prediction_expert_system(id):
    idFromUrl = getIdFromUrl(id)
    form = SendToPredictionExpertSystem()
    patient = baseService.get_by_id(class_=Patient, id=idFromUrl)
    if form.validate_on_submit():
        patientFacade.update_patient(form=form, id=getIdFromUrl(id))
        resultFacade.send_to_prediction_expert_system(patient=patient, id=idFromUrl, form=form)
        flash(f'Pacient {form.nameOfPatient.data} byl úspěšně odeslán k predikci', 'success')
        return redirect(url_for('patientsBp.patient', id=idFromUrl))
    resultFacade.prepare_data_for_form(form=form, id=idFromUrl, patient=patient)
    return render_template("prediction/sendToPredictionExpertSystem.html",
                           title="Predikce pomoci expertního systému a NS",
                           form=form, patient_id=idFromUrl)

from typing import List

from emphysema import db


class BaseService:

    def get_by_id(self, class_, id):
        return db.session.query(class_).get(id)

    def get_all(self, class_):
        return db.session.query(class_).all()

    def get_by_name(self, class_, name):
        return db.session.query(class_).filter_by(name)

    def save(self, _object):
        db.session.add(_object)
        db.session.commit()

    def bulk_save(self, _objects):
        db.session.bulk_save_objects(_objects)
        db.session.commit()

    def update(self):
        db.session.commit()

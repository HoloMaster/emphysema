def getIdFromUrl(stringedId) -> int:
    return int(stringedId.split("-")[0])


def getNameFromUrl(stringedId) -> str:
    return stringedId.split("-")[1]

def getPopulateDiseaseFromUrl(stringedId):
    return stringedId.split("-")

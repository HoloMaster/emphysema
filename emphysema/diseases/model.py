from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

from emphysema import db

Base = declarative_base()


class Disease(db.Model, Base):
    __tablename__ = "tbl_disease"

    id = db.Column(db.Integer, primary_key=True)

    name_of_disease = db.Column(db.String(150),
                                nullable=False, unique=True)

    patientToDisease = relationship("DiseaseToPatient", cascade="all")

    disease_to_symptom = relationship("DiseaseToSymptom", cascade="all")

    def __init__(self, name_of_disease):
        self.name_of_disease = name_of_disease

    def __repr__(self):
        return f"Disease('{self.name_of_disease}', '{self.patientToDisease}','{self.disease_to_symptom}')"


class DiseaseToPatient(db.Model, Base):
    __tablename__ = "tbl_diseases_to_patient"

    id = db.Column(db.Integer, primary_key=True)
    patient_id = db.Column('patient_id', db.Integer, db.ForeignKey('tbl_patient.id'))
    disease_id = db.Column('name_of_disease', db.String(150), db.ForeignKey('tbl_disease.name_of_disease'))

    def __init__(self, disease_id):
        self.disease_id = disease_id

    def __repr__(self):
        return f"DiseaseToPatient('{self.id}', '{self.disease_id}')"

import logging

from flask import Blueprint, render_template, flash, redirect, url_for, request
from flask_login import login_required

from emphysema.diseases.facade import DiseaseFacade
from emphysema.diseases.froms import DiseaseForm
from emphysema.diseases.manager import DiseaseService
from emphysema.services.crudService import BaseService
from emphysema.services.utils import getIdFromUrl
from emphysema.routes import Diseases, DiseasesForUrl, DiseaseCreate

diseasesBp = Blueprint('diseasesBp', __name__)


baseService = BaseService()
diseaseService = DiseaseService()
diseaseFacade = DiseaseFacade()

@diseasesBp.route('/diseases')
@login_required
def diseases():
    page = request.args.get('page', 1, type=int)
    per_page = request.args.get('per_page', 20, type=int)
    diseases_form_db = diseaseService.get_paged_diseases(page=page,per_page=per_page)
    return render_template(Diseases, title="Nemoci", diseases=diseases_form_db)


@diseasesBp.route('/createDisease', methods=['GET', 'POST'])
@login_required
def create_disease():
    form = DiseaseForm()
    if form.validate_on_submit():
        try:
            diseaseFacade.create_disease(form=form)
            flash(f'Nemoc {form.name_of_disease.data} vytvořen úspěšně', 'success')
        except Exception as e:
            flash(f'Během vytváření nemoci {form.name_of_disease.data} vznikla chyba, '
                  f'zkontrolujte, zdali daná nemoc již neexistuje.', 'danger')
            logging.error(e)
        finally:
            return redirect(url_for('diseasesBp.diseases'))
    return render_template(DiseaseCreate, title="Vytvoření nemoci", form=form)


@diseasesBp.route('/removeDisease/<id>')
@login_required
def remove_disease(id):
    diseaseService.remove(id=getIdFromUrl(id))
    return redirect(url_for(DiseasesForUrl))

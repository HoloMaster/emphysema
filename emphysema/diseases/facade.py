from emphysema.diseases.froms import DiseaseForm
from emphysema.diseases.model import Disease
from emphysema.diseases.manager import DiseaseService

diseaseService = DiseaseService()


class DiseaseFacade:

    @staticmethod
    def create_disease(form: DiseaseForm):
        disease = Disease(name_of_disease=form.name_of_disease.data)
        diseaseService.save(disease)

    @staticmethod
    def update_disease(form: DiseaseForm, id: int):
        disease = diseaseService.get_by_id(class_=Disease, id=id)
        disease.name_of_disease = form.name_of_disease.data
        diseaseService.update()

    @staticmethod
    def populate_form(form: DiseaseForm, id: int):
        disease = diseaseService.get_by_id(class_=Disease, id=id)
        form.set_diseases_for_update_form(form=form, disease=disease)
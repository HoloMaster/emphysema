from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import Length, DataRequired


class DiseaseForm(FlaskForm):
    name_of_disease = StringField("Jméno nemoci", validators=[DataRequired(), Length(1, 255)])

    submit = SubmitField("Uložit")

    def set_diseases_for_update_form(self, form, disease):
        form.name_of_disease.data = disease.name_of_disease

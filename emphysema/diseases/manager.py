from emphysema import db
from emphysema.diseases.model import DiseaseToPatient
from emphysema.diseases.model import Disease
from emphysema.services.crudService import BaseService


class DiseaseService(BaseService):
    def __init__(self) -> None:
        super().__init__()



    def get_by_patient(self, name):
        return DiseaseToPatient.query.with_entities(DiseaseToPatient.disease_id).filter(DiseaseToPatient.patient_id == name).all()


    def get_diseases_selected(self, diseases):
        return Disease.query.filter(Disease.name_of_disease.in_(diseases)).all()

    def get_paged_diseases(self, page: int, per_page: int):
        return Disease.query.paginate(page=page, per_page=per_page)

    def remove(self,id: int):
        Disease.query.filter_by(id=id).delete()
        db.session.commit()


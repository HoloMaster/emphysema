import logging
from flask import Flask
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy

from emphysema.config import Config

db = SQLAlchemy()
logging.basicConfig()
#logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)
login_manager = LoginManager()
login_manager.login_view = 'usersBp.login'


def create_aplication(config=Config):
    STATIC_FOLDER = 'static'
    STATIC_FOLDER_URL = '/static'
    TEMPLATES_FOLDER = 'templates'
    app = Flask(__name__, static_folder=STATIC_FOLDER,
                static_url_path=STATIC_FOLDER_URL,
                template_folder=TEMPLATES_FOLDER)
    app.config.from_object(Config)

    db.init_app(app)
    login_manager.init_app(app)

    from emphysema.users.controller import usersBp
    from emphysema.patients.controller import patientsBp
    from emphysema.results.controller import resultsBp
    from emphysema.diseases.controller import diseasesBp
    from emphysema.symptoms.controller import symptomBp
    from emphysema.main.controller import main

    app.register_blueprint(usersBp)
    app.register_blueprint(patientsBp)
    app.register_blueprint(resultsBp)
    app.register_blueprint(diseasesBp)
    app.register_blueprint(symptomBp)
    app.register_blueprint(main)

    return app

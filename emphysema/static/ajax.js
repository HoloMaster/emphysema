$(document).on('click', '.pagedResultsOnPacient', function (event) {
    event.preventDefault()
    $.ajax({
        url: $(this).attr('href'),
        data: 'html',
        success: function (response) {
            $('#results').replaceWith(response);
        }
    });
})

$(document).on('click','#xRayModalGet', function (event) {
    event.preventDefault()
    let result_id = $(event.currentTarget).data('val');
    $.ajax({
        url: '/result_x_ray/'+result_id,
        data: 'html',
        success: function (response) {
            $('#imgXray').replaceWith(response);
        }
    });
})

$(document).ajaxSend(function () {
    $('.own').fadeIn(10);
});
$(document).ajaxComplete(function () {
    $('.own').fadeOut(10);
});

$(document).on('click','#collapsingAdvancedMenu', function (event) {
    event.preventDefault()
    $( ".advancedMenu" ).toggle("slow");
});
$(document).ready(function () {
    $( ".alert-dismissible" ).delay( 4000 ).fadeOut(2300, "linear");
});

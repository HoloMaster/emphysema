create table tbl_disease
(
	id int auto_increment
		primary key,
	name_of_disease varchar(150) not null,
	constraint name_of_disease
		unique (name_of_disease)
)
charset=utf8mb4;

create table tbl_patient
(
	id int auto_increment
		primary key,
	nameOfPatient varchar(255) not null,
	height int not null,
	weight int not null,
	smoker tinyint(1) null,
	pulse_oxymetry int null,
	ptf_fev1 int null,
	ptf_dlco int null,
	age int null,
	breath_frequency int null,
	cigarets_per_year int null,
	ptf_fcv int null,
	deficit_alfa_1 tinyint(1) null
);

create table tbl_diseases_to_patient
(
	id int auto_increment
		primary key,
	patient_id int null,
	name_of_disease varchar(150) null,
	constraint tbl_diseases_to_patient_ibfk_1
		foreign key (patient_id) references tbl_patient (id),
	constraint tbl_diseases_to_patient_ibfk_2
		foreign key (name_of_disease) references tbl_disease (name_of_disease)
)
charset=utf8mb4;

create index name_of_disease
	on tbl_diseases_to_patient (name_of_disease);

create index patient_id
	on tbl_diseases_to_patient (patient_id);

create table tbl_resultoftest
(
	id int auto_increment
		primary key,
	dateOfCreation datetime not null,
	dateOfEnd datetime null,
	emphysemaNN decimal(10,2) null,
	emphysemaES varchar(255) null,
	pathToXray varchar(255) null,
	originalName varchar(255) null,
	generatedName varchar(255) null,
	patient_id int null,
	constraint tbl_resultoftest_ibfk_1
		foreign key (patient_id) references tbl_patient (id)
)
charset=utf8mb4;

create index patient_id
	on tbl_resultoftest (patient_id);

create table tbl_symptoms
(
	id int auto_increment
		primary key,
	name_of_symptom varchar(150) not null,
	constraint name_of_symptom
		unique (name_of_symptom)
)
charset=utf8mb4;

create table tbl_disease_to_symptom
(
	id int auto_increment
		primary key,
	name_of_disease varchar(150) null,
	name_of_symptom varchar(150) null,
	constraint tbl_disease_to_symptom_ibfk_1
		foreign key (name_of_disease) references tbl_disease (name_of_disease),
	constraint tbl_disease_to_symptom_ibfk_2
		foreign key (name_of_symptom) references tbl_symptoms (name_of_symptom)
)
charset=utf8mb4;

create index name_of_disease
	on tbl_disease_to_symptom (name_of_disease);

create index name_of_symptom
	on tbl_disease_to_symptom (name_of_symptom);

create table tbl_patient_to_symptom
(
	id int auto_increment
		primary key,
	patient_id int null,
	name_of_symptom varchar(150) null,
	constraint tbl_patient_to_symptom_ibfk_1
		foreign key (patient_id) references tbl_patient (id),
	constraint tbl_patient_to_symptom_ibfk_2
		foreign key (name_of_symptom) references tbl_symptoms (name_of_symptom)
)
charset=utf8mb4;

create index name_of_symptom
	on tbl_patient_to_symptom (name_of_symptom);

create index patient_id
	on tbl_patient_to_symptom (patient_id);

create table tbl_user
(
	user_id int auto_increment
		primary key,
	username varchar(255) not null,
	email varchar(40) not null,
	password varchar(200) not null,
	constraint email
		unique (email)
)
charset=utf8mb4;

commit;
import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    SQLALCHEMY_DATABASE_URI = os.environ.get('CLEARDB_DATABASE_URL')
    SQLALCHEMY_ENGINE_OPTIONS = \
        {"pool_recycle": 280,
         "pool_timeout": 10,
         "pool_pre_ping": True}
    SECRET_KEY = "6a466804ed8d5d2883f2b958a3297ef8"
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True

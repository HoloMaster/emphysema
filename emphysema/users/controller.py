import secrets

from flask import Blueprint, redirect, render_template, url_for
from flask_login import login_user, logout_user, login_required

from emphysema import db
from emphysema.users.froms import LoginForm, UserForm
from emphysema.users.model import User

usersBp = Blueprint('usersBp', __name__)


@usersBp.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(password=form.hashed_login.data).first()
        if user:
            login_user(user=user, remember=form.remember.data)
            return redirect(url_for('main.index'))
    return render_template("login.html", title="Login", form=form)


@usersBp.route('/createUser', methods=['GET', 'POST'])
@login_required
def saveUser():
    form = UserForm()
    if form.validate_on_submit():
        # hashed_password = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
        user = User(username=form.user_name.data, email=str(secrets.token_hex(2)), password=form.password.data)
        db.session.add(user)
        db.session.commit()
        return redirect(url_for('main.index'))
    form.password.data = str(secrets.token_hex(32))
    return render_template("createUser.html", title="Vytvoření uživtele", form=form)


@usersBp.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('usersBp.login'))

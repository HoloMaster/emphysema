from flask_wtf import FlaskForm
from wtforms import StringField, BooleanField, SubmitField
from wtforms.validators import Length, DataRequired


class LoginForm(FlaskForm):
    hashed_login = StringField("Hash uživatele", validators=[DataRequired(), Length(min=3, max=255)])

    remember = BooleanField("Zapamatovat")

    submit = SubmitField("Přihlásit se")


class UserForm(FlaskForm):
    user_name = StringField("Jméno uživatele", validators=[DataRequired(), Length(min=1, max=255)])

    password = StringField("Heslo")

    submit = SubmitField("Vytvořit")

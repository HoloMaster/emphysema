from emphysema.models.patient import Patient
from emphysema.models.resultOfTest import ResultOfTest

from emphysema import db


def get_result_for_patient(petient_id: int, page: int, per_page: int):
    return db.session.query(ResultOfTest, Patient). \
        select_from(ResultOfTest).join(Patient). \
        filter_by(id=petient_id). \
        paginate(page=page, per_page=per_page)


def get_results(page: int, per_page: int):
    return db.session.query(ResultOfTest, Patient). \
        select_from(ResultOfTest).join(Patient). \
        paginate(page=page, per_page=per_page)

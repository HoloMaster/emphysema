from flask_login import UserMixin

from emphysema import db, login_manager


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


class User(db.Model, UserMixin):
    __tablename__ = "tbl_user"

    user_id = db.Column(db.Integer,
                        primary_key=True)
    username = db.Column(db.String(255),
                         nullable=False,
                         unique=False)
    email = db.Column(db.String(40),
                      unique=True,
                      nullable=False)
    password = db.Column(db.String(200),
                         primary_key=False,
                         unique=False,
                         nullable=False)

    def __init__(self, username, email, password):
        self.email = email
        self.password = password
        self.username = username

    def get_id(self):
        return self.user_id

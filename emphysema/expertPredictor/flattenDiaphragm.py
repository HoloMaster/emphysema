import os

import pytorch_lightning as pl
import torch
import torchvision
from PIL import Image
from flask import current_app
from torch import nn
from torch.nn import functional as F
from torchvision import transforms, datasets
import math as m


class FlattenDrPred(pl.LightningModule):

    def __init__(self, num_target_classes):
        super().__init__()
        self.save_hyperparameters()
        self.model = torchvision.models.resnet50(pretrained=False)
        num_ftrs = self.model.fc.in_features
        self.model.fc = nn.Linear(num_ftrs, num_target_classes)
        self.acc = pl.metrics.Accuracy()

    def forward(self, x):
        return self.model(x)

    def training_step(self, batch, batch_idx):
        x, y = batch
        y_hat = self(x)

        loss = F.cross_entropy(y_hat, y)
        self.log('train_loss_flatten', loss)
        self.log('train_acc_step_flatten', self.acc(y_hat, y), on_step=True, on_epoch=False)

        return loss

    def training_epoch_end(self, outs):
        acc = self.acc.compute()
        self.log("train_acc_epoch_flatten", acc)

    def validation_step(self, batch, batch_idx):
        x, y = batch

        y_hat = self(x)

        loss = F.cross_entropy(y_hat, y)
        self.log('val_loss_flatten', loss)

    def configure_optimizers(self):
        optimizer = torch.optim.SGD(self.parameters(), lr=0.01)
        return optimizer


def get_model_for_predicition():
    return FlattenDrPred.load_from_checkpoint(os.path.join(current_app.root_path, 'static/model', 'model_flatten.ckpt'))





class DataPredictor():

    classes = ['flatten_diaphragm', 'no_findings']

    def __init__(self):
        super().__init__()

    def dataPrepare(self, size, path):
        img = Image.open(path)
        w, h = img.size
        left = 0
        top = m.floor(h / 2)
        right = w
        bottom = h
        img.crop((left, top, right, bottom))
        transformer = transforms.Compose([
            transforms.Resize((size, size)),
            transforms.ToTensor(),
            transforms.Grayscale(num_output_channels=3),
            transforms.Normalize(0.5, 0.5)
        ])
        transformed_img = transformer(img)
        batch_t = torch.unsqueeze(transformed_img, 0)
        return batch_t

    def predict(self, predictor, size, path):
        batch_t = self.dataPrepare(size=size, path=path)
        predictor.eval()
        out = predictor(batch_t)
        _, index = torch.max(out, 1)
        percentage = torch.nn.functional.softmax(out, dim=1)[0] * 100
        del predictor
        return self.classes[index[0]], percentage[index[0]].item()

from emphysema.expertPredictor.expertClass import PredictIllness, PulseOxymetryFromPatient, \
    PtfDlcoFromPatient, PtfFev1FromPatient, BodyMassIndexFromPatient, EmphysemaFromPatient, PredictionFromNNForPatient
from emphysema.patients.utils import compute_bmi


class Handler:

    def __init__(self, patient, results, type_of_prediction=None):
        self.patient = patient,
        self.results = results
        self.type_of_prediction = type_of_prediction

    def __repr__(self):
        return f"RestHandler('{self.patient}', '{self.results}')"

    @staticmethod
    def notify(symptomsForEmphysemaFact, diseasesFact, numAndDiseaseFact, ptfFev1FromPatient,
               ptfDlcoFromPatient, pulseOxymetryFromPatient, patientBMI, smoker, result):
        predictor = PredictIllness()
        predictor.reset()
        predictor.declare(
            PulseOxymetryFromPatient(pulseOxymetryFromPatient),
            PtfFev1FromPatient(ptfFev1FromPatient),
            PtfDlcoFromPatient(ptfDlcoFromPatient),
            BodyMassIndexFromPatient(patientBMI),
            EmphysemaFromPatient(smoker),
            PredictionFromNNForPatient(result),
            symptomsForEmphysemaFact,
            *diseasesFact,
            *numAndDiseaseFact
        )
        print(predictor.facts)
        predictor.run()
        return predictor.result

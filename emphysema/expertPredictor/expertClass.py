import logging

from experta import *


class Emphysema(Fact):
    smoker = Field(bool)


class PulseOxymetry(Fact):
    minPulseOxymetry = Field(int, default=95)


class PtfFev1Mild(Fact):
    maxValue = 100
    minValue = 70


class PtfFev1Moderate(Fact):
    maxValue = 34
    minValue = 0


class PtfFev1ModeratelySevere(Fact):
    maxValue = Field(int, default=69)
    minValue = Field(int, default=60)


class PtfFev1Severe(Fact):
    maxValue = Field(int, default=59)
    minValue = Field(int, default=50)


class PtfFev1VerySevere(Fact):
    maxValue = Field(int, default=49)
    minValue = Field(int, default=0)


class PtfDlcoMild(Fact):
    maxValue = Field(int, default=79)
    minValue = Field(int, default=60)


class PtfDlcoModerate(Fact):
    maxValue = Field(int, default=59)
    minValue = Field(int, default=40)


class PtfDlcoVerySevere(Fact):
    maxValue = Field(int, default=39)
    minValue = Field(int, default=0)


class EmphysemaFromPatient(Fact):
    pass


class PtfFev1FromPatient(Fact):
    pass


class PtfDlcoFromPatient(Fact):
    pass


class PulseOxymetryFromPatient(Fact):
    pass


class BodyMassIndex(Fact):
    underweight = Field(float, default=18.5)
    normal_weight_min = Field(float, default=18.5)
    normal_weight_max = Field(float, default=24.9)
    overweight_min = Field(float, default=25)
    overweight_max = Field(float, default=29.9)
    obesity = Field(float, default=30)


class BreathFrequency(Fact):
    maxValue = Field(int, default=20)
    minValue = Field(int, default=15)


class Age(Fact):
    young = Field(int, default=30)


class CigaretPerYear(Fact):
    value = Field(int, default=20)


class DeficitAlfa1(Fact):
    value = Field(bool, default=True)


class PredictionFromNN(Fact):
    value = Field(int, default=50)


class BreathFrequencyFromPatient(Fact):
    pass


class AgeFromPatient(Fact):
    pass


class CigaretPerYearFromPatient(Fact):
    pass


class DeficitAlfa1FromPatient(Fact):
    pass


class BodyMassIndexFromPatient(Fact):
    pass


class Symptoms(Fact):
    pass


class Diseases(Fact):
    pass


class SymptomsToDisease(Fact):
    pass


class Prediction(Fact):
    pass


class PredictionFromNNForPatient(Fact):
    pass


def increment(symptoms, disease):
    matches = 0
    for _, disease_to_symptom in symptoms:
        if disease_to_symptom == disease:
            matches += 1
    if len(symptoms) == matches:
        logging.info("Number of symptons for disease and patient symptons matches")
        return True


class PredictIllness(KnowledgeEngine):
    result = ''

    @DefFacts()
    def init_emphysema_facts(self):
        yield Emphysema(smoker=True)
        yield PtfFev1VerySevere(maxValue=49, minValue=0)
        yield PtfFev1Severe(maxValue=59, minValue=50)
        yield PtfFev1ModeratelySevere(maxValue=69, minValue=60)
        yield PtfFev1Moderate()
        yield PtfFev1Mild()
        yield PtfDlcoVerySevere(maxValue=39, minValue=0)
        yield PtfDlcoModerate(maxValue=59, minValue=40)
        yield PtfDlcoMild(maxValue=79, minValue=60)
        yield BodyMassIndex(underweight=18.5, normal_weight_min=18.5, normal_weight_max=24.9, overweight_min=25.0,
                            overweight_max=29.9, obesity=30.0)
        yield PulseOxymetry(minPulseOxymetry=95)
        yield Age(young=30)
        yield BreathFrequency(maxValue=20, minValue=15)
        yield DeficitAlfa1(value=True)
        yield CigaretPerYear(value=20)
        yield PredictionFromNN(value=50)

    @Rule(Symptoms(val=MATCH.symptoms),
          SymptomsToDisease(num=MATCH.all_symptoms_for_disease, disease=MATCH.disease),
          TEST(lambda symptoms, disease: increment(symptoms, disease))
          )
    def detect_disease_0(self, symptoms, all_symptoms_for_disease, disease):
        if len(symptoms) == all_symptoms_for_disease:
            self.result = disease
            self.declare(Prediction('matches_all_symptoms'))

    @Rule(AND(Prediction('matches_all_symptoms'),
              NOT(Prediction('FEV1_decreased'))),
          PtfFev1FromPatient(MATCH.FEV1),
          TEST(lambda FEV1: FEV1 is not None),
          PtfFev1VerySevere(maxValue=MATCH.FEV1VSMAX, minValue=MATCH.FEV1VSMIN),
          PtfFev1Severe(maxValue=MATCH.FEV1SMAX, minValue=MATCH.FEV1SMIN),
          OR(AND(TEST(lambda FEV1, FEV1VSMAX: FEV1 <= FEV1VSMAX),
                 TEST(lambda FEV1, FEV1VSMIN: FEV1 >= FEV1VSMIN)),
             AND(TEST(lambda FEV1, FEV1SMAX: FEV1 <= FEV1SMAX),
                 TEST(lambda FEV1, FEV1SMIN: FEV1 >= FEV1SMIN)))
          )
    def detect_disease_1(self, FEV1):
        self.result += "\r\n Velmi nízké hodnoty FEV1/VC " + str(FEV1)
        self.declare(Prediction('FEV1_decreased'))

    @Rule(NOT(Prediction('FEV1_decreased')),
          PtfFev1FromPatient(MATCH.FEV1),
          TEST(lambda FEV1: FEV1 is not None),
          PtfFev1VerySevere(maxValue=MATCH.FEV1VSMAX, minValue=MATCH.FEV1VSMIN),
          PtfFev1Severe(maxValue=MATCH.FEV1SMAX, minValue=MATCH.FEV1SMIN),
          OR(AND(TEST(lambda FEV1, FEV1VSMAX: FEV1 <= FEV1VSMAX),
                 TEST(lambda FEV1, FEV1VSMIN: FEV1 >= FEV1VSMIN)),
             AND(TEST(lambda FEV1, FEV1SMAX: FEV1 <= FEV1SMAX),
                 TEST(lambda FEV1, FEV1SMIN: FEV1 >= FEV1SMIN)))
          )
    def detect_disease_2(self, FEV1):
        self.result += "\r\n Velmi nízké hodnoty FEV1 " + str(FEV1)
        self.declare(Prediction('FEV1_decreased'))

    @Rule(AND(NOT(Prediction('matches_all_symptoms')), NOT(Prediction('LOW_FEV1_DLCO')), Prediction('FEV1_decreased'),
              Prediction('DLCO_decreased')),
          PtfDlcoFromPatient(MATCH.DLCO),
          PtfFev1FromPatient(MATCH.FEV1)
          )
    def detect_disease_3(self, FEV1, DLCO):
        self.result = "\r\n Velmi nízké hodnoty FEV1 {} a DLCO {} \r\n -> indikátor Emfyzému".format(FEV1, DLCO)
        self.declare(Prediction('LOW_FEV1_DLCO'))

    @Rule(Prediction('FEV1_decreased'),
          PtfDlcoFromPatient(MATCH.DLCO),
          TEST(lambda DLCO: DLCO is not None),
          PtfDlcoVerySevere(maxValue=MATCH.DLCOVSMAX, minValue=MATCH.DLCOVSMIN),
          AND(TEST(lambda DLCO, DLCOVSMAX: DLCO <= DLCOVSMAX),
              TEST(lambda DLCO, DLCOVSMIN: DLCO >= DLCOVSMIN))
          )
    def detect_disease_4(self, DLCO):
        self.result += "\r\n Velmi nízké hodnoty DLCO " + str(DLCO)
        self.declare(Prediction('DLCO_decreased'))

    @Rule(AND(Prediction('matches_all_symptoms'), Prediction('LOW_FEV1_DLCO')),
          PulseOxymetryFromPatient(MATCH.OXYGEN),
          TEST(lambda OXYGEN: OXYGEN is not None),
          PulseOxymetry(minPulseOxymetry=MATCH.minPulseOxymetry),
          TEST(lambda OXYGEN, minPulseOxymetry: OXYGEN < minPulseOxymetry)
          )
    def detect_disease_5(self, OXYGEN):
        self.result += "\r\n Nižší hodnoty SpO2 " + str(OXYGEN)
        self.declare(Prediction('SpO2_decrease'))

    @Rule(NOT(Prediction('smoker')),
          CigaretPerYear(MATCH.CPY),
          CigaretPerYearFromPatient(MATCH.CPYFP),
          TEST(lambda CPYFP: CPYFP is not None),
          TEST(lambda CPY, CPYFP: CPYFP > CPY)
          )
    def detect_disease_6(self):
        self.declare(Prediction('smoker'))

    @Rule(NOT(Prediction('bmi_checked')),
          BodyMassIndexFromPatient(MATCH.BMI),
          TEST(lambda BMI: BMI is not None),
          BodyMassIndex(obesity=MATCH.obesity),
          TEST(lambda obesity, BMI: BMI > obesity)
          )
    def detect_disease_7(self, BMI):
        self.result += "\r\n Vysoké hodnoty BMI " + str(BMI)
        self.declare(Prediction('bmi_checked_high'))

    # Checking BMI of Patient
    @Rule(NOT(Prediction('bmi_checked')),
          BodyMassIndexFromPatient(MATCH.BMI),
          TEST(lambda BMI: BMI is not None),
          BodyMassIndex(overweight_min=MATCH.overweight_min, overweight_max=MATCH.overweight_max),
          AND(TEST(lambda overweight_min, BMI: BMI >= overweight_min),
              TEST(lambda overweight_max, BMI: BMI <= overweight_max))
          )
    def detect_disease_8(self, BMI):
        self.result += "\r\n Vyšší hodnoty BMI " + str(BMI)
        self.declare(Prediction('bmi_checked'))
        self.declare(Prediction('bmi_checked_higher'))

    @Rule(NOT(Prediction('bmi_checked')),
          BodyMassIndexFromPatient(MATCH.BMI),
          TEST(lambda BMI: BMI is not None),
          BodyMassIndex(normal_weight_min=MATCH.normal_weight_min, normal_weight_max=MATCH.normal_weight_max),
          AND(TEST(lambda normal_weight_min, BMI: BMI >= normal_weight_min),
              TEST(lambda normal_weight_max, BMI: BMI <= normal_weight_max))
          )
    def detect_disease_9(self, BMI):
        self.result += "\r\n Normální hodnoty BMI " + str(BMI)
        self.declare(Prediction('bmi_checked'))
        self.declare(Prediction('bmi_checked_norm'))

    @Rule(NOT(Prediction('bmi_checked')),
          BodyMassIndexFromPatient(MATCH.BMI),
          TEST(lambda BMI: BMI is not None),
          BodyMassIndex(underweight=MATCH.underweight),
          TEST(lambda underweight, BMI: BMI < underweight)
          )
    def detect_disease_10(self, BMI):
        self.result += "\r\n Nízké hodnoty BMI " + str(BMI)
        self.declare(Prediction('bmi_checked'))
        self.declare(Prediction('bmi_checked_low'))

    @Rule(NOT(Prediction('breathe_frq')),
          BreathFrequencyFromPatient(MATCH.BRTHFQ),
          TEST(lambda BRTHFQ: BRTHFQ is not None),
          BreathFrequency(maxValue=MATCH.BRTHFQMAX, minValue=MATCH.BRTHFQMIN),
          OR(TEST(lambda BRTHFQ, BRTHFQMAX: BRTHFQ > BRTHFQMAX),
             TEST(lambda BRTHFQ, BRTHFQMIN: BRTHFQ < BRTHFQMIN))
          )
    def detect_disease_11(self, BRTHFQ):
        self.result += "\r\n Pacient má potíže s dýcháním " + str(BRTHFQ)
        self.declare(Prediction('breathe_frq_not_norm'))

    # Checking deficit alfa 1 for match, only for young people (age < 30)
    @Rule(NOT(Prediction('NN_result_for_young_positive')),
          AgeFromPatient(MATCH.AGE),
          PredictionFromNNForPatient(MATCH.NN),
          DeficitAlfa1FromPatient(MATCH.DA1),
          TEST(lambda AGE: AGE is not None),
          TEST(lambda NN: NN is not None),
          TEST(lambda DA1: DA1 is not None),
          DeficitAlfa1(MATCH.ALFAVALUE),
          PredictionFromNN(MATCH.NNVALUE),
          Age(MATCH.AGEVAULUE),
          AND(TEST(lambda AGE, AGEVAULUE: AGE <= AGEVAULUE),
              TEST(lambda NN, NNVALUE: NN >= NNVALUE),
              TEST(lambda DA1, ALFAVALUE: DA1 is True))
          )
    def detect_disease_12(self):
        self.declare(Prediction('NN_result_for_young_positive'))

    @Rule(AND(NOT(Prediction('NN_result_for_young_negative')),
              NOT(Prediction('NN_result_for_young_positive'))),
          AgeFromPatient(MATCH.AGE),
          PredictionFromNNForPatient(MATCH.NN),
          DeficitAlfa1FromPatient(MATCH.DA1),
          TEST(lambda AGE: AGE is not None),
          TEST(lambda NN: NN is not None),
          TEST(lambda DA1: DA1 is not None),
          PredictionFromNN(MATCH.NNVALUE),
          Age(MATCH.AGEVAULUE),
          AND(TEST(lambda AGE, AGEVAULUE: AGE <= AGEVAULUE),
              TEST(lambda NN, NNVALUE: NN >= NNVALUE),
              TEST(lambda DA1: DA1 is False or DA1 is None))
          )
    def detect_disease_13(self):
        self.declare(Prediction('NN_result_for_young_negative'))

    @Rule(AND(Prediction('matches_all_symptoms'), Prediction('LOW_FEV1_DLCO'), Prediction('SpO2_decrease'),
              Prediction('smoker'), Prediction('breathe_frq_not_norm')),
          AgeFromPatient(MATCH.AGE),
          PredictionFromNNForPatient(MATCH.NN),
          TEST(lambda AGE: AGE is not None),
          TEST(lambda NN: NN is not None),
          PredictionFromNN(MATCH.NNVALUE),
          Age(MATCH.AGEVAULUE),
          AND(TEST(lambda AGE, AGEVAULUE: AGE > AGEVAULUE),
              TEST(lambda NN, NNVALUE: NN < NNVALUE))
          )
    def detect_disease_14(self):
        self.declare(Prediction('NN_maybe_not_reliable_result'))

    @Rule(NOT(Prediction('NN_measured_higher')),
          PredictionFromNNForPatient(MATCH.NN),
          TEST(lambda NN: NN is not None),
          PredictionFromNN(MATCH.NNVALUE),
          TEST(lambda NN, NNVALUE: NN > NNVALUE)
          )
    def detect_disease_14(self):
        self.declare(Prediction('NN_measured_higher'))

    @Rule(NOT(Prediction('NN_measured_lower')),
          PredictionFromNNForPatient(MATCH.NN),
          TEST(lambda NN: NN is not None),
          PredictionFromNN(MATCH.NNVALUE),
          TEST(lambda NN, NNVALUE: NN < NNVALUE)
          )
    def detect_disease_15(self):
        self.declare(Prediction('NN_measured_lower'))

    @Rule(AND(Prediction('matches_all_symptoms'), Prediction('LOW_FEV1_DLCO'), Prediction('SpO2_decrease'),
              Prediction('bmi_checked_low'), Prediction('smoker'), Prediction('NN_result_for_young_positive'),
              Prediction('breathe_frq_not_norm'), Prediction('NN_measured_higher'))
          )
    def detect_disease_16(self):
        self.result += "<strong> Pacient splňuje veškeré požadavky pro nemoc: Emphysema </strong>"

    @Rule(Prediction('NN_maybe_not_reliable_result')
          )
    def detect_disease_17(self):
        self.result += "<strong> Výstup z NS je nutné přehodnotit" \
                      "\r\n z důvodu ostatních dat o pacientovi </strong>"

    @Rule(Prediction('NN_result_for_young_negative')
          )
    def detect_disease_18(self):
        self.result += "<strong> Pacient je mladý a netrpí alfa-1-antitrypsináza," \
                      "\r\n výsledek z NS může být zkreslen </strong>"


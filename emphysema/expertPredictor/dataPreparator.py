import json
from email.mime import image


class DataPreparator:
    image
    sizeOfImage = (256, 256)

    def __init__(self, patient, sizeOfImage, diseasesToBePredicted, image=None):
        self.image = image
        self.sizeOfImage = sizeOfImage

    def exportOtherRequests(self, otherDiseases, patient):
        return json.dump([patient.patient_id])

    def prepareDiseaseForNN(self, DI):
        return DI

from sqlalchemy.ext.declarative import declarative_base

from emphysema import db
from emphysema.diseases.model import DiseaseToPatient
from emphysema.symptoms.model import SymptomToPatient

Base = declarative_base()


class Patient(db.Model, Base):
    __tablename__ = "tbl_patient"

    id = db.Column(db.Integer,
                   primary_key=True)

    nameOfPatient = db.Column(db.String(255),
                              nullable=False,
                              unique=False)
    height = db.Column(db.Integer,
                       unique=False,
                       nullable=False)
    weight = db.Column(db.Integer,
                       unique=False,
                       nullable=False)
    pulse_oxymetry = db.Column(db.Integer,
                               unique=False,
                               nullable=True)
    ptf_fev1 = db.Column(db.Integer,
                         unique=False,
                         nullable=True)
    ptf_dlco = db.Column(db.Integer,
                         unique=False,
                         nullable=True)
    smoker = db.Column(db.Boolean, unique=False, nullable=True)

    age = db.Column(db.Integer,
                    unique=False,
                    nullable=True)

    cigarets_per_year = db.Column(db.Integer,
                                  unique=False,
                                  nullable=True)

    deficit_alfa_1 = db.Column(db.Boolean, unique=False, nullable=True)

    breath_frequency = db.Column(db.Integer,
                                 unique=False,
                                 nullable=True)

    ptf_fcv = db.Column(db.Integer,
                        unique=False,
                        nullable=True)

    otherDiseases = db.relationship("DiseaseToPatient", lazy=True, cascade="all, delete, delete-orphan")

    results = db.relationship('ResultOfTest', backref='tbl_patient', lazy=True, cascade="all, delete, delete-orphan")

    symptoms = db.relationship("SymptomToPatient", lazy=True, cascade="all, delete, delete-orphan")

    def __init__(self, nameOfPatient, height, weight, pulse_oxymetry, ptf_fev1, ptf_dlco, ptf_fcv, smoker,
                 otherDiseases, symptoms, age, breath_frequency, cigarets_per_year, deficit_alfa_1, results=[]):
        if results is None:
            results = []
        self.height = height
        self.nameOfPatient = nameOfPatient
        self.weight = weight
        self.pulse_oxymetry = pulse_oxymetry
        self.ptf_fev1 = ptf_fev1
        self.ptf_dlco = ptf_dlco
        self.ptf_fcv = ptf_fcv
        self.age = age
        self.breath_frequency = breath_frequency
        self.cigarets_per_year = cigarets_per_year
        self.deficit_alfa_1 = deficit_alfa_1
        self.smoker = smoker
        self.otherDiseases = otherDiseases
        self.symptoms = symptoms
        self.results = results

    def __repr__(self):
        return f"Patient('{self.nameOfPatient}', '{self.height}', '{self.weight}', '{self.smoker}', '{self.results}', '{self.otherDiseases}')"

    def set_form_data_for_update(self, form, patient, diseases_to_patient):
        patient.nameOfPatient = form.nameOfPatient.data
        patient.height = form.height.data
        patient.weight = form.weight.data
        patient.pulse_oxymetry = form.pulse_oxymetry.data
        patient.ptf_fev1 = form.ptf_fev1.data
        patient.ptf_dlco = form.ptf_dlco.data
        patient.smoker = form.smoker.data
        patient.age = form.age.data
        patient.breath_frequency = form.breath_frequency.data
        patient.cigarets_per_year = form.cigarets_per_year.data
        patient.deficit_alfa_1 = form.deficit_alfa_1.data
        patient.ptf_fcv = form.ptf_fcv.data
        patient.otherDiseases = [DiseaseToPatient(disease_id=name.name_of_disease) for name in diseases_to_patient]
        patient.symptoms = [SymptomToPatient(symptom_id=name) for name in form.symptoms.data]

    def __str__(self) -> str:
        return super().__str__()

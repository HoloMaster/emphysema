from emphysema.diseases.manager import DiseaseService
from emphysema.diseases.model import Disease, DiseaseToPatient
from emphysema.patients.froms import PatientForm
from emphysema.patients.manager import PatientService
from emphysema.patients.model import Patient
from emphysema.symptoms.manager import SymptomService
from emphysema.symptoms.model import Symptom, SymptomToPatient

patientsService = PatientService()
diseaseService = DiseaseService()
symptomService = SymptomService()


class PatientFacade:

    @staticmethod
    def crete_new_patient(form: PatientForm):
        symptoms_to_patient = symptomService.get_symptoms_for_patient(symptoms=form.symptoms.data)
        diseases_to_patient = diseaseService.get_diseases_selected(diseases=form.otherDiseases.data)
        patient = Patient(nameOfPatient=form.nameOfPatient.data, height=form.height.data, weight=form.weight.data,
                          ptf_fev1=form.ptf_fev1.data, ptf_dlco=form.ptf_dlco.data,
                          pulse_oxymetry=form.pulse_oxymetry.data,
                          smoker=form.smoker.data, age=form.age.data, ptf_fcv=form.ptf_fcv.data,
                          breath_frequency=form.breath_frequency.data, deficit_alfa_1=form.deficit_alfa_1.data,
                          cigarets_per_year=form.cigarets_per_year.data,
                          otherDiseases=[DiseaseToPatient(disease_id=name.name_of_disease) for name in
                                         diseases_to_patient],
                          symptoms=[SymptomToPatient(symptom_id=name.name_of_symptom) for name in symptoms_to_patient])
        patientsService.save(patient)

    @staticmethod
    def populate_data_for_form_register(form: PatientForm):
        form.set_diseases_in_creation_form(form, diseases=diseaseService.get_all(class_=Disease))
        form.set_symptoms_in_creation_form(form, symptoms=symptomService.get_all(class_=Symptom))

    @staticmethod
    def update_patient(form: PatientForm, id: int):
        diseases_to_patient = diseaseService.get_diseases_selected(diseases=form.otherDiseases.data)
        patient = patientsService.get_by_id(class_=Patient, id=id)
        patient.set_form_data_for_update(form=form, patient=patient,
                                         diseases_to_patient=diseases_to_patient)

        patientsService.update()

    @staticmethod
    def populate_data_for_form_update(form: PatientForm, id: int):
        patient_with_diseases = patientsService.get_all_for_user(patient_id=id)
        if patient_with_diseases:
            diseases = diseaseService.get_all(class_=Disease)
            symptoms = symptomService.get_all(class_=Symptom)
            form.set_patient_for_update(form=form, patient=patient_with_diseases, diseases=diseases, symptoms=symptoms)
        else:
            patient = patientsService.get_by_id(class_=Patient, id=id)
            form.set_patient_for_update_without_disease(form=form, patient=patient)

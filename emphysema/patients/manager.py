from typing import List

from sqlalchemy.orm import joinedload

from emphysema.diseases.model import DiseaseToPatient
from emphysema.patients.model import Patient
from emphysema.services.crudService import BaseService
from emphysema import db


class PatientService(BaseService):

    def get_paged_patients(self, page: int, per_page: int) -> List[tuple]:
        return Patient.query.paginate(page=page, per_page=per_page)

    def get_all_for_user(self, patient_id: int):
        return Patient.query\
            .options(joinedload(Patient.otherDiseases))\
            .options(joinedload(Patient.symptoms)) \
            .filter(Patient.id == patient_id).all()

    def remove(self, id: int):
        DiseaseToPatient.query.filter_by(patient_id=id).delete()
        patient = Patient.query.get(id)
        db.session.delete(patient)
        db.session.commit()

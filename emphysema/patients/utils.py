def compute_bmi(weight, height):
    return round(weight / pow(height / 100, 2), 1)


def compute_fe1vc(fev1, fcv):
    if fev1 and fcv:
        return round((fev1 / fcv) * 100)

from flask import Blueprint, render_template, flash, redirect, url_for, request
from flask_login import login_required

from emphysema import db
from emphysema.diseases.model import Disease, DiseaseToPatient
from emphysema.diseases.manager import DiseaseService
from emphysema.patients.facade import PatientFacade
from emphysema.patients.froms import PatientForm
from emphysema.patients.model import Patient
from emphysema.patients.manager import PatientService
from emphysema.results.service import get_result_for_patient, get_results
from emphysema.services.utils import getIdFromUrl
from emphysema.services.crudService import BaseService
from emphysema.symptoms.model import Symptom, SymptomToPatient
from emphysema.symptoms.manager import SymptomService

patientsBp = Blueprint('patientsBp', __name__)

patientsService = PatientService()
diseaseService = DiseaseService()
symptomService = SymptomService()
baseService = BaseService()
patientFacade = PatientFacade()


@patientsBp.route('/patients')
@login_required
def patients():
    page = request.args.get('page', 1, type=int)
    per_page = request.args.get('per_page', 20, type=int)
    patients = patientsService.get_paged_patients(page=page, per_page=per_page)
    return render_template("patient/patients.html", title="Pacienti", patients=patients)


@patientsBp.route('/patient/<id>')
@login_required
def patient(id):
    page = request.args.get('page', 1, type=int)
    per_page = request.args.get('per_page', 20, type=int)
    patient_detail = patientsService.get_by_id(class_=Patient, id=getIdFromUrl(id))
    results = get_result_for_patient(id, page, per_page)
    return render_template("patient/patient.html", title="Pacienti", patient=patient_detail, results=results)


@patientsBp.route('/patient/resultPaged')
@login_required
def pagedResultsForPatient():
    page = request.args.get('page', 1, type=int)
    per_page = request.args.get('per_page', 20, type=int)
    results = get_results(page, per_page)
    return render_template("result/resultsForPaginationOnPatient.html", results=results)


@patientsBp.route('/registerNewPatient', methods=['GET', 'POST'])
@login_required
def registerNewPatient():
    form = PatientForm()
    if form.validate_on_submit():
        patientFacade.crete_new_patient(form=form)
        flash(f'Pacient {form.nameOfPatient.data} vytvořen úspěšně', 'success')
        return redirect(url_for('patientsBp.patients'))
    patientFacade.populate_data_for_form_register(form=form)
    return render_template("patient/registerNewPatient.html",
                           title="Registrace nového pacienta",
                           form=form,
                           button="Vytvořit uživatele",
                           legend="Nový pacient"
                           )


@patientsBp.route('/registerNewPatient/<id>', methods=['GET', 'POST'])
@login_required
def updatePatient(id):
    form = PatientForm()
    if form.validate_on_submit():
        patientFacade.update_patient(form=form,id=getIdFromUrl(id))
        flash(f'Pacient {form.nameOfPatient.data} aktualizován úspěšně', 'success')
        return redirect(url_for('patientsBp.patients'))

    patientFacade.populate_data_for_form_update(form=form,id=getIdFromUrl(id))
    patientFacade.populate_data_for_form_register(form=form)
    return render_template("patient//registerNewPatient.html",
                           title="Aktualizace pacienta",
                           form=form,
                           button="Aktualizovat uživatele",
                           legend="Aktualizace pacienta")


@patientsBp.route('/removePatient/<id>')
@login_required
def remove_patient(id):
    patientsService.remove(getIdFromUrl(id))
    flash(f'Pacient úspěšně smazán', 'success')
    return redirect(url_for('patientsBp.patients'))

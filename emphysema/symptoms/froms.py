from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import Length, DataRequired

from emphysema.forms.customForms.selectMultipleFiledCustom import SelectMultipleFieldCustom


class SymptomForm(FlaskForm):
    name_of_symptom = StringField("Jméno symptomu", validators=[DataRequired(), Length(1, 255)])

    symptom_to_disease = SelectMultipleFieldCustom("Nemoci které se vztahují k symptomu", validators=[])

    submit = SubmitField("Uložit")

    def set_symptom_for_update_form(self, form, symptom):
        form.name_of_symptom.data = symptom.name_of_symptom

    def set_diseases_in_creation_form(self, form, diseaseSelected, diseases):
        if diseases:
            form.symptom_to_disease.choices = [(d.name_of_disease, d.name_of_disease) for d in diseases]
            form.symptom_to_disease.process_data(d.name_of_disease for d in diseaseSelected)

from sqlalchemy import func

from emphysema import db
from emphysema.services.crudService import BaseService
from emphysema.symptoms.model import SymptomToPatient, Symptom, DiseaseToSymptom


class SymptomService(BaseService):
    def __init__(self) -> None:
        super().__init__()

    def get_by_symptom_to_patient_name(self, name):
        return SymptomToPatient.query.filter(SymptomToPatient.symptom_id == name).all()

    def get_by_symptom_name(self, name):
        return Symptom.query.filter(Symptom.name_of_symptom == name).all()

    def get_by_patient(self, name):
        return SymptomToPatient.query.filter(SymptomToPatient.patient_id == name).all()

    def get_symptoms_for_patient(self, symptoms):
        return Symptom.query.filter(Symptom.name_of_symptom.in_(symptoms)).all()

    def get_symptoms_for_disease_patient(self, patient_id, diseases=["Emphysema"]):
        return DiseaseToSymptom.query.with_entities(DiseaseToSymptom.symptom_id,DiseaseToSymptom.disease_id) \
            .join(Symptom) \
            .join(SymptomToPatient) \
            .filter(DiseaseToSymptom.disease_id.in_(diseases)) \
            .filter(SymptomToPatient.patient_id == patient_id) \
            .all()

    def get_symptoms_for_disease(self, disease):
        return db.session.query(func.count(DiseaseToSymptom.symptom_id)) \
            .filter(DiseaseToSymptom.disease_id == disease) \
            .scalar()


    def get_paged_symptoms(self, page: int, per_page: int):
        return Symptom.query.paginate(page=page, per_page=per_page)

    def remove(self, id: int):
        Symptom.query.filter_by(id=id).delete()
        db.session.commit()

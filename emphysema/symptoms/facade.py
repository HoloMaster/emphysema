from emphysema.diseases.froms import DiseaseForm
from emphysema.diseases.manager import DiseaseService
from emphysema.diseases.model import Disease
from emphysema.symptoms.manager import SymptomService
from emphysema.symptoms.model import Symptom, DiseaseToSymptom

symptomService = SymptomService()
diseaseService = DiseaseService()


class SymptomFacade:

    @staticmethod
    def create_symptom(form: DiseaseForm):
        diseases_selected = diseaseService.get_diseases_selected(diseases=form.symptom_to_disease.data)
        if not symptomService.get_by_symptom_name(name=form.name_of_symptom.data):
            symptom = Symptom(name_of_symptom=form.name_of_symptom.data,
                              disease_to_symptom=[
                                  DiseaseToSymptom(symptom_id=form.name_of_symptom.data,
                                                   disease_id=name.name_of_disease)
                                  for name in
                                  diseases_selected])
            symptomService.save(symptom)
        else:
            symptomService.bulk_save([
                DiseaseToSymptom(symptom_id=form.name_of_symptom.data, disease_id=name.name_of_disease)
                for name in
                diseases_selected])

    @staticmethod
    def populate_form(form: DiseaseForm, id: int):
        disease = symptomService.get_by_id(class_=Disease, id=id)
        form.set_diseases_for_update_form(form=form, disease=disease)

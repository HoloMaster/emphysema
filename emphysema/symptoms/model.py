from sqlalchemy.ext.declarative import declarative_base
from emphysema import db

Base = declarative_base()


class Symptom(db.Model, Base):
    __tablename__ = "tbl_symptoms"

    id = db.Column(db.Integer, primary_key=True)

    name_of_symptom = db.Column(db.String(150),
                                nullable=False, unique=True)

    disease_to_symptom = db.relationship("DiseaseToSymptom", cascade="all, delete, delete-orphan")

    def __init__(self, name_of_symptom, disease_to_symptom):
        self.name_of_symptom = name_of_symptom
        self.disease_to_symptom = disease_to_symptom

    def __repr__(self):
        return f"Symptom('{self.name_of_symptom}')"


class SymptomToPatient(db.Model, Base):
    __tablename__ = "tbl_patient_to_symptom"

    id = db.Column(db.Integer, primary_key=True)
    patient_id = db.Column('patient_id', db.Integer, db.ForeignKey('tbl_patient.id'))
    symptom_id = db.Column('name_of_symptom', db.String(150), db.ForeignKey('tbl_symptoms.name_of_symptom'))

    def __init__(self, symptom_id):
        self.symptom_id = symptom_id

    def __repr__(self):
        return f"SymptomToPatient('{self.id}', '{self.symptom_id}')"


class DiseaseToSymptom(db.Model, Base):
    __tablename__ = "tbl_disease_to_symptom"

    id = db.Column(db.Integer, primary_key=True)
    disease_id = db.Column('name_of_disease', db.String(150), db.ForeignKey('tbl_disease.name_of_disease'))
    symptom_id = db.Column('name_of_symptom', db.String(150), db.ForeignKey('tbl_symptoms.name_of_symptom'))

    def __init__(self, disease_id, symptom_id):
        self.disease_id = disease_id
        self.symptom_id = symptom_id

    def __repr__(self):
        return f"DiseaseToSymptom('{self.id}', '{self.symptom_id}', '{self.disease_id}')"

from flask import Blueprint, render_template, flash, redirect, url_for, request
from flask_login import login_required

from emphysema.diseases.manager import DiseaseService
from emphysema.diseases.model import Disease
from emphysema.services.crudService import BaseService
from emphysema.services.utils import getIdFromUrl, getNameFromUrl, getPopulateDiseaseFromUrl
from emphysema.symptoms.facade import SymptomFacade
from emphysema.symptoms.froms import SymptomForm
from emphysema.symptoms.manager import SymptomService

symptomBp = Blueprint('symptomBp', __name__)

baseService = BaseService()
symptomService = SymptomService()
symptomFacade = SymptomFacade()
diseaseService = DiseaseService()


@symptomBp.route('/symptoms')
@login_required
def symptoms():
    page = request.args.get('page', 1, type=int)
    per_page = request.args.get('per_page', 20, type=int)
    symptoms_form_db = symptomService.get_paged_symptoms(page=page, per_page=per_page)
    return render_template('symptom/symptoms.html', title="Symptomy", symptoms=symptoms_form_db)


@symptomBp.route('/createSymptom', methods=['GET', 'POST'])
@login_required
def create_symptom():
    form = SymptomForm()
    if form.validate_on_submit():
        symptomFacade.create_symptom(form=form)
        flash(f'Symptom {form.name_of_symptom.data} vytvořen úspěšně', 'success')
        return redirect(url_for('symptomBp.symptoms'))
    form.set_diseases_in_creation_form(form, diseases=diseaseService.get_all(class_=Disease))
    return render_template("symptom/createSymptom.html", title="Vytvoření symptomu", form=form)


@symptomBp.route('/createSymptom/<id>', methods=['GET', 'POST'])
@login_required
def create_symptom_for_disease(id):
    form = SymptomForm()
    if form.validate_on_submit():
        symptomFacade.create_symptom(form=form)
        flash(f'Symptom {form.name_of_symptom.data} vytvořen úspěšně pro nemoc {getNameFromUrl(stringedId=id)}', 'success')
        return redirect(url_for('symptomBp.symptoms'))
    form.set_diseases_in_creation_form(form,
                                       diseases=diseaseService.get_all(class_=Disease),
                                       diseaseSelected=diseaseService.get_diseases_selected(
                                           getPopulateDiseaseFromUrl(stringedId=id)))
    return render_template("symptom/createSymptom.html", title="Vytvoření symptomu", form=form)


@symptomBp.route('/removeSymptom/<id>')
@login_required
def remove_symptom(id):
    symptomService.remove(id=getIdFromUrl(id))
    return redirect(url_for('symptomBp.symptoms'))
